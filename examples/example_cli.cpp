#include "irc++/line.hpp"

#include <iostream>
#include <cstdlib>

int main(int argc, const char **argv)
{
	if(argc < 2)
	{
		std::cerr << "Error: need a line as a parameter" << std::endl;
		std::cerr << "Usage: " << argv[0] << " \"line\"" << std::endl;
		return EXIT_FAILURE;
	}

	irc::line::RFCLine test(argv[1]);

	std::cout << "Tags: ";

	irc::line::RFCTags &tags = *test.tags;
	for(auto &p : tags)
		std::cout << '"' << p.first << "\" : \"" << p.second << "\"; ";

	std::cout << std::endl;

	irc::line::RFCHostmask &hostmask = *test.hostmask;
	std::cout << "Nickname: \"" << hostmask.nick << "\"" << std::endl;
	std::cout << "Username: \"" << hostmask.user << "\"" << std::endl;
	std::cout << "Hostname: \"" << hostmask.host << "\"" << std::endl;
	std::cout << std::endl;
	std::cout << "Command: \"" << test.command << "\"" << std::endl;
	std::cout << "Parameters: ";

	for(auto p : test.parameters)
		std::cout << "\"" << p << "\" ";

	std::cout << std::endl;

	return EXIT_SUCCESS;
}
