/**
 * @file
 *
 * A simple wrapper for POSIX sockets
 */

#ifndef IRCPP_SOCKET_HPP
#define IRCPP_SOCKET_HPP

#include <system_error>
#include <utility>
#include <cerrno>

extern "C"
{
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
}

/**
 * A simple wrapper around POSIX sockets.
 */
class Socket
{
private:
	int fd = -1;
public:
	/**
	 * Return the file descriptor associated with this object
	 */
	int get_fd() { return fd; }

	/**
	 * Connect the underlying file descriptor
	 *
	 * @param addr `sockaddr` to connect to
	 * @param addrlen Length of `sockaddr` structure
	 */
	void connect(const sockaddr *addr, socklen_t &addrlen)
	{
		if(::connect(fd, addr, addrlen) < 0)
			throw std::system_error(errno, std::system_category());
	}

	/**
	 * Listen on the given file descriptor.
	 *
	 * It is advisable to call `bind` on the socket before listening.
	 *
	 * @param backlog Maximum length of the queue where connections not yet `accept`ed are kept
	 */
	void listen(int backlog = 4)
	{
		if(::listen(fd, backlog) < 0)
			throw std::system_error(errno, std::system_category());
	}

	using AcceptReturn = std::tuple<Socket, sockaddr_storage>;

	/**
	 * Accept a connection from a listening socket.
	 *
	 * It is advisable to call `listen` on the socket before accepting.
	 */
	AcceptReturn accept()
	{
		int fd;
		sockaddr_storage storage;
		socklen_t len = sizeof(sockaddr_storage);

		if((fd = ::accept(fd, reinterpret_cast<sockaddr *>(&storage), &len)) < 0)
			throw std::system_error(errno, std::system_category());

		return std::make_tuple(std::move(Socket(fd)), std::move(storage));
	}

	/**
	 * Bind to a given address and port specified by a `sockaddr` structure.
	 *
	 * @param addr A `sockaddr` containing address and port to listen on.
	 * @param addrlen Length of the `addr` param
	 */
	void bind(const sockaddr *addr, socklen_t &addrlen)
	{
		if(::bind(fd, addr, addrlen) < 0)
			throw std::system_error(errno, std::system_category());
	}

	/**
	 * Send data on the socket.
	 *
	 * @param data Bytes to send onto the socket
	 * @param size Number of bytes to send
	 * @param flags Flags to send the data with (optional)
	 */
	ssize_t send(const char *data, std::size_t size, int flags = 0)
	{
		ssize_t ret;
		if((ret = ::send(fd, data, size, flags)) < 0)
			throw std::system_error(errno, std::system_category());

		return ret;
	}

	/**
	 * Send data on the socket to a given peer.
	 *
	 * This is most useful for UDP sockets.
	 *
	 * @param data Bytes to send onto the socket
	 * @param size Number of bytes to send
	 * @param flags Flags to send the data with
	 * @param addr `sockaddr` structure with IP and port to send data to
	 * @param addrlen length of `addr` data
	 */
	ssize_t sendto(const char *data, std::size_t size, int flags,
		const sockaddr &addr, const socklen_t &addrlen)
	{
		ssize_t ret;
		if((ret = ::sendto(fd, data, size, flags, &addr, addrlen)) < 0)
			throw std::system_error(errno, std::system_category());

		return ret;
	}

	/**
	 * Send data on the socket to a given peer, without flags.
	 *
	 * This is most useful for UDP sockets.
	 *
	 * @param data Bytes to send onto the socket
	 * @param size Number of bytes to send
	 * @param addr `sockaddr` structure with IP and port to send data to
	 * @param addrlen length of `addr` data
	 */
	ssize_t sendto(const char *data, std::size_t size, const sockaddr &addr,
		const socklen_t &addrlen)
	{
		return sendto(data, size, 0, addr, addrlen);
	}

	/**
	 * Recieve data data from the socket.
	 *
	 * @param data Pointer to location where data is received from the socket
	 * @param size Number of bytes to receive
	 * @param flags Flags to send the data with (optional)
	 */
	ssize_t recv(char *data, std::size_t size, int flags = 0)
	{
		ssize_t ret;
		if((ret = ::recv(fd, data, size, flags)) < 0)
			throw std::system_error(errno, std::system_category());

		return ret;
	}

	/**
	 * Receive data from the socket to a given peer.
	 *
	 * This is most useful for UDP sockets.
	 *
	 * @param data Pointer to location where data is received from the socket
	 * @param size Number of bytes to receive
	 * @param flags Flags to receive the data with
	 * @param addr `sockaddr` structure with IP and port to receive data from
	 * @param addrlen length of `addr` data
	 */
	ssize_t recvfrom(char *data, std::size_t size, int flags,
		sockaddr &addr, socklen_t &addrlen)
	{
		ssize_t ret;
		if((ret = ::recvfrom(fd, data, size, flags, &addr,
			&addrlen)) < 0)
		{
			throw std::system_error(errno, std::system_category());
		}

		return ret;
	}

	/**
	 * Receive data from the socket to a given peer, without flags.
	 *
	 * This is most useful for UDP sockets.
	 *
	 * @param data Pointer to location where data is received from the socket
	 * @param size Number of bytes to receive
	 * @param addr `sockaddr` structure with IP and port to receive data from
	 * @param addrlen length of `addr` data
	 */
	ssize_t recvfrom(char *data, std::size_t size,
		sockaddr &addr, socklen_t &addrlen)
	{
		return recvfrom(data, size, 0, addr, addrlen);
	}

	/**
	 * Set a given socket option with a value
	 *
	 * This calls `setsockopt()` with the parameters given.
	 *
	 * @tparam T Type of data to set
	 * @param opt Option to set
	 * @param value Value to set
	 * @param size Size of parameter we are setting
	 */
	template<typename T>
	void setsockopt(int opt, const T value, size_t size = sizeof(T))
	{
		static_assert(std::is_arithmetic<T>::value,
			"Only arithmetic types or strings are supported");

		if(::setsockopt(fd, SOL_SOCKET, opt, &value, size) < 0)
			throw std::system_error(errno, std::system_category());
	}

	/**
	 * Get a given socket option
	 *
	 * This calls `getsockopt()` with the parameters given.
	 *
	 * @tparam T Type to receive data into
	 * @param opt Option to set
	 * @param value Pointer to where we are storing data
	 * @param size Size of parameter we are storing data into
	 */
	template<typename T>
	void getsockopt(int opt, T *value, size_t size = sizeof(T))
	{
		if(::getsockopt(fd, SOL_SOCKET, opt, value, size) < 0)
			throw std::system_error(errno, std::system_category());
	}

	/**
	 * Change SO_REUSEADDR flag on socket
	 *
	 * @param reuse Setting for SO_REUSEADDR
	 */
	void reuseaddr(bool reuse = true)
	{
		setsockopt(SO_REUSEADDR, reuse ? 1 : 0);
	}

	/**
	 * Change SO_BROADCAST flag on socket
	 *
	 * @param set Setting for SO_BROADCAST
	 */
	void broadcast(bool set = true)
	{
		setsockopt(SO_BROADCAST, set ? 1 : 0);
	}

	/**
	 * Set the socket blocking or non-blocking.
	 *
	 * @param blocking Whether or not the socket will be in blocking mode.
	 */
	void setblocking(bool blocking)
	{
		int flags = ::fcntl(fd, F_GETFL);
		if(flags < 0)
			throw std::system_error(errno, std::system_category());

		if(blocking)
			flags &= ~O_NONBLOCK;
		else
			flags |= O_NONBLOCK;

		if(::fcntl(fd, F_SETFL, flags) < 0)
			throw std::system_error(errno, std::system_category());
	}

	/**
	 * Close the underlying socket.
	 *
	 * This action cannot be undone, and the socket cannot be reopened by design.
	 */
	void close()
	{
		::close(fd);
		fd = -1;
	}

	Socket & operator=(const Socket &other)
	{
		fd = ::dup(other.fd);
		return *this;
	}

	Socket & operator=(Socket &&other)
	{
		fd = other.fd;
		other.fd = -1;
		return *this;
	}

	Socket() = default;
	Socket(const Socket &other) : fd(::dup(other.fd)) {}
	Socket(Socket &&other) : fd(other.fd) { other.fd = -1; }

	/**
	 * Create a socket from an existing file descriptor
	 */
	explicit Socket(int fd) : fd(fd) {}

	/**
	 * Create a socket with the given domain, packet type, and protocol
	 */
	explicit Socket(int domain, int type, int protocol)
	{
		if((fd = socket(domain, type, protocol)) < 0)
			throw std::system_error(errno, std::system_category());
	}

	virtual ~Socket()
	{
		if(fd >= 0)
			close();
	}
};

#endif //IRCPP_SOCKET_HPP
