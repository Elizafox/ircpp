/**
 * @file
 *
 * A simple `getaddrinfo` wrapper.
 */

#ifndef IRCPP_GETADDRINFO_HPP
#define IRCPP_GETADDRINFO_HPP

#include <iostream>
#include <system_error>
#include <utility>
#include <cerrno>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>


/**
 * A higher-level wrapper around `struct addrinfo`.
 */
class AddrInfo
{
private:
	struct addrinfo *next;	///< Internal pointer around `ai_next`
public:
	int flags = 0;		///< Equivalent to `ai_flags`
	int addr_family = 0;	///< Equivalent to `ai_family`
	int socket_type = 0;	///< Equivalent to `ai_socktype`
	int protocol = 0;	///< Equivalent to `ai_protocol`
	socklen_t address_len = 0;	///< Equivalent to `ai_addrlen`
	sockaddr *address = nullptr;	///< Equivalent to `ai_addr`
	char *canonical_name = nullptr;	///< Equivalent to `ai_canonname`

	/**
	 * Create an `addrinfo` struct from this structure
	 */
	const addrinfo to_addrinfo()
	{
		addrinfo addr = {};

		addr.ai_flags = flags;
		addr.ai_family = addr_family;
		addr.ai_socktype = socket_type;
		addr.ai_protocol = protocol;
		addr.ai_addrlen = address_len;
		addr.ai_addr = address;
		addr.ai_canonname = canonical_name;
		addr.ai_next = next;

		return addr;
	}

	/**
	 * Create a human-readable integer (IP family) and IP pair.
	 */
	std::pair<int, std::string> humanise()
	{
		char str_addr[INET6_ADDRSTRLEN] = {};
		void *sock_ptr;

		if(addr_family == AF_INET)
			sock_ptr = &(((sockaddr_in *)address)->sin_addr);
		else
			sock_ptr = &(((sockaddr_in6 *)address)->sin6_addr);

		inet_ntop(addr_family, sock_ptr, str_addr, sizeof(str_addr));

		return std::make_pair(addr_family, std::string(str_addr));
	}

	AddrInfo(struct addrinfo &ai) :
		next(ai.ai_next), flags(ai.ai_flags), addr_family(ai.ai_family),
		socket_type(ai.ai_socktype), protocol(ai.ai_protocol),
		address_len(ai.ai_addrlen), address(ai.ai_addr),
		canonical_name(ai.ai_canonname)
	{}
	AddrInfo(struct addrinfo *ai) : AddrInfo(*ai) {}
	AddrInfo() = default;
};

/**
 * Wrapper class around `getaddrinfo`.
 *
 * This class provides `begin` and `end` iterators to retrieve results,
 * returning `AddrInfo` instances.
 */
class GetAddrInfo
{
private:
	addrinfo *result;
	addrinfo hints;

	inline void do_getaddrinfo()
	{
		int res = getaddrinfo(node.c_str(), service.c_str(),
			&hints, &result);
		if(res != 0)
			throw std::system_error(res, std::generic_category(),
				gai_strerror(res));
	}
public:
	std::string node;	///< Node we are peforming lookup on
	std::string service;	///< Service we are performing lookup for

	/**
	 * Create `getaddrinfo` object.
	 *
	 * @param node Node to look up
	 * @param service Service to use in lookup (may be blank)
	 * @param hints Hints to pass into `getaddrinfo`.
	 * @throws std::system_error Errors from `gai_error`.
	 */
	explicit GetAddrInfo(const std::string &node,
		const std::string &service, const addrinfo &hints) :
		hints(hints), node(node), service(service)
	{
		do_getaddrinfo();
	}

	explicit GetAddrInfo(const std::string &node,
		const std::string &service, AddrInfo &hints) :
		GetAddrInfo(node, service, hints.to_addrinfo()) {}

	GetAddrInfo(GetAddrInfo &&other) :
		result(other.result), hints(std::move(other.hints)),
		node(std::move(other.node)), service(std::move(other.service))
	{
		// The other object's dead
		other.result = nullptr;
	}

	GetAddrInfo(const GetAddrInfo &other) :
		GetAddrInfo(other.node, other.service, other.hints) {}

	~GetAddrInfo()
	{
		if(result != nullptr)
			freeaddrinfo(result);
	}

	GetAddrInfo & operator=(const GetAddrInfo &other)
	{
		if(result != nullptr)
			freeaddrinfo(result);

		node = other.node;
		service = other.service;
		hints = other.hints;

		do_getaddrinfo();
		return *this;
	}

	GetAddrInfo & operator=(GetAddrInfo &&other)
	{
		if(other.result != nullptr)
		{
			freeaddrinfo(other.result);
			other.result = nullptr;
		}

		node = std::move(other.node);
		service = std::move(other.service);
		hints = std::move(other.hints);

		do_getaddrinfo();
		return *this;
	}

	template<typename T>
	class Iterator
	{
	private:
		addrinfo *addr;
		AddrInfo cur; // A temporary to hold a reference
	public:
		using self_type = Iterator<T>;
		using difference_type = std::ptrdiff_t;
		using value_type = T;
		using pointer = T *;
		using reference = T &;
		using iterator_category = std::forward_iterator_tag;

		bool operator==(self_type &other) const
		{
			return addr == other.addr;
		}

		bool operator!=(self_type &other) const
		{
			return addr != other.addr;
		}

		self_type & operator++()
		{
			addr = addr->ai_next;
			return *this;
		}

		self_type operator++(int)
		{
			self_type &ret = *this;
			++(*this);
			return ret;
		}

		reference operator*()
		{
			cur = AddrInfo(addr);
			return cur;
		}

		Iterator(addrinfo *addr) : addr(addr) {}
	};

	using iterator = Iterator<AddrInfo>;
	using const_iterator = Iterator<const AddrInfo>;

	iterator begin() { return iterator(result); }
	iterator end() { return iterator(nullptr); }
	const_iterator cbegin() const { return const_iterator(result); }
	const_iterator cend() const { return const_iterator(nullptr); }
};

#endif //IRCPP_GETADDRINFO_HPP
