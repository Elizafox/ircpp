#include <iostream>
#include <array>
#include <memory>
#include <algorithm>
#include <string>

#include "irc++/line.hpp"
#include "irc++/connection.hpp"
#include "irc++/numerics.hpp"
#include "irc++/iostream.hpp"
#include "example-bot/socket.hpp"
#include "example-bot/getaddrinfo.hpp"

/**
 * Our socket connection class, designed to work with our bespoke socket class above.
 */
class SocketConnection : public irc::connection::Connection
{
private:
	Socket sock;
	std::string buf;
public:
	void send_data(const std::string &data)
	{
		std::cout << "OUT: " << data;

		const char *dpos = data.c_str();
		ssize_t length = data.size();

		while(length != 0)
		{
			ssize_t ret;
			if((ret = sock.send(dpos, length)) <= 0)
				return;

			length -= ret;
			dpos += ret;
		}
	}

	void read_data()
	{
		std::array<char, 8192> rbuf;
		ssize_t length;

		// Read data into buffer
		if((length = sock.recv(rbuf.data(), rbuf.size())) == 0)
			throw "Connection Terminated";

		buf.append(rbuf.begin(), rbuf.begin() + length);

		size_t pos;
		while((pos = buf.find("\r\n")) != std::string::npos)
		{
			pos += 2;
			std::string data(buf.begin(), buf.begin() + pos);
			buf.erase(0, pos);

			irc::line::RFCLine line(data);
			execute_in(line);
		}
	}

	SocketConnection() = default;
	explicit SocketConnection(Socket &sock) : sock(sock) {}
	explicit SocketConnection(Socket &&sock) : sock(std::move(sock)) {}
	SocketConnection(const SocketConnection &other) = delete;
	SocketConnection(SocketConnection &&other) : sock(std::move(other.sock)) {}
};

/**
 * Registers our connection as successful and joins a test channel
 */
class WelcomeAction : public irc::action::Action
{
private:
	SocketConnection &state;
public:
	void execute_registered(irc::line::Line &line)
	{
		state.set_registered();

		state.send_message("JOIN", {"#test"});

	}

	WelcomeAction(SocketConnection &state) : state(state) {}
};

/**
 * Responds to ping replies
 */
class PingAction : public irc::action::Action
{
private:
	SocketConnection &state;

	void execute(irc::line::Line &line)
	{
		state.send_message("PONG", *line.parameters);
	}
public:
	inline void execute_unregistered(irc::line::Line &line)
	{
		return execute(line);
	}

	inline void execute_registered(irc::line::Line &line)
	{
		return execute(line);
	}

	PingAction(SocketConnection &state) : state(state) {}
};

/**
 * Displays lines to standard output
 */
class CoutAction : public irc::action::Action
{
private:
	SocketConnection &state;
public:
	void execute_registered(irc::line::Line &line)
	{
		std::cout << "UNREG: " << line;
	}

	void execute_unregistered(irc::line::Line &line)
	{
		std::cout << "REG: " << line;
	}

	CoutAction(SocketConnection &state) : state(state) {}
};

/**
 * Create a SocketConnection instance from host and port
 */
SocketConnection connect(const std::string host, const std::string port)
{
	addrinfo hints;

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	GetAddrInfo g(host, port, hints);

	for(auto &res : g)
	{
		try
		{
			Socket s(res.addr_family, res.socket_type, res.protocol);
			s.connect(res.address, res.address_len);
			return SocketConnection(std::move(s));
		}
		catch(std::system_error &e)
		{
		}
	}

	throw "Could not establish connection";
}

int main(int argc, const char **argv)
{
	auto sc = connect(argv[1], argv[2]);

	// Add these
	WelcomeAction welcome(sc);
	PingAction ping(sc);
	CoutAction out(sc);

	sc.add_action_in(irc::numerics::RPL_WELCOME, welcome);
	sc.add_action_in("PING", ping);
	sc.add_action_in(irc::action::ACTION_ANY, out);

	sc.send_message("NICK", {"Testbot"});
	sc.send_message("USER", {"Testbot", "*", "*", "Test bot"});

	try
	{
		while(true)
			sc.read_data();
	}
	catch(const char *e)
	{
		std::cerr << e << std::endl;
	}

	return 0;
}
