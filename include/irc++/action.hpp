/**
 * @file irc++/action.hpp
 *
 * Handling of actions
 */

#ifndef IRCPP_ACTION_HPP
#define IRCPP_ACTION_HPP

#include "irc++/util.hpp"

#include <atomic>
#include <string>
#include <unordered_map>
#include <functional>
#include <utility>
#include <list>

#include "irc++/line.hpp"
#include "irc++/exceptions.hpp"

namespace irc
{
	namespace action
	{
		/**
		 * A special command name that causes an action to always run
		 */
		const std::string ACTION_ANY("");

		/**
		 * The base class for actions, which attach code to received
		 * and sent lines.
		 */
		class IRCPP_EXPORT Action
		{
		private:
			friend class ActionTable;

			static std::atomic<int64_t> gid;
			int64_t uid;

			inline void assign_uid() { uid = gid++; }
		public:
			/**
			 * Implementation-defined action when registered
			 */
			virtual void execute_registered(line::Line &line)
			{
				throw RegisteredOnlyException();
			}

			/**
			 * Implementation-defined action when unregistered
			 */
			virtual void execute_unregistered(line::Line &line)
			{
				throw UnregisteredOnlyException();
			}

			/**
			 * Get the action UID.
			 */
			int64_t get_uid() { return uid; }

			Action & operator=(const Action &) = default;
			Action & operator=(Action &&other)
			{
				uid = other.uid;

				// Invalidated
				other.uid = -1;

				return *this;
			}

			Action() { assign_uid(); }
			Action(const Action &action) = default;
			Action(Action &&other) noexcept : uid(other.uid)
			{
				// Invalidated
				other.uid = -1;
			}

			virtual ~Action() {}
		};

		/**
		 * A table that stores individual actions
		 *
		 * @warning Do not allow inserted actions to go out of scope or be freed or you will
		 *          be sorry!
		 */
		class IRCPP_EXPORT ActionTable
		{
		private:
			using action_list = std::list<std::reference_wrapper<Action>>;

			std::unordered_map<std::string, action_list> actions;
		public:
			void add_action(std::string command, Action &action);
			void add_action_before(std::string command, Action &action,
					const Action &before);
			void add_action_after(std::string command, Action &action,
					const Action &after);

			void del_action(std::string command);
			void del_action(std::string command, Action &action);

			void execute_registered(line::Line &line);
			void execute_unregistered(line::Line &line);
		};
	}
}

#endif // IRCPP_ACTION_HPP
