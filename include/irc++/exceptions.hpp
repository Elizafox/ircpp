/**
 * @file irc++/exceptions.hpp
 *
 * @brief irc++ exception definitions
 *
 * This file contains exception definitions. Eventually these exceptions will
 * be moved to more appropriate locations.
 */

#ifndef IRCPP_EXCEPTIONS_HPP
#define IRCPP_EXCEPTIONS_HPP

#include "irc++/util.hpp"

namespace irc
{
	/**
	 * Virtual base class for all irc++ exceptions
	 */
	class IRCPP_EXPORT IRCException : public std::exception
	{
	public:
		virtual const char * what() const throw() = 0;
	};

	namespace action
	{
		class IRCPP_EXPORT ActionException : public IRCException
		{
			virtual const char * what() const throw()
			{
				return "Exception during action";
			}
		};

		class IRCPP_EXPORT UnregisteredOnlyException : public ActionException
		{
			virtual const char * what() const throw()
			{
				return "Action only valid on registered clients";
			}
		};

		class IRCPP_EXPORT RegisteredOnlyException : public ActionException
		{
			virtual const char * what() const throw()
			{
				return "Action only valid on unregistered clients";
			}
		};
	}

	namespace line
	{
		/**
		 * Base exception for errors during parsing.
		 */
		class IRCPP_EXPORT ParseException : public IRCException
		{
		public:
			virtual const char * what() const throw()
			{
				return "Exception during parsing";
			}
		};

		/**
		 * Base exception for all errors encountered whilst parsing tags
		 */
		class IRCPP_EXPORT TagException : public ParseException
		{
		public:
			const std::string tag;
			const std::string value;

			virtual const char * what() const throw()
			{
				return "Tag exception";
			}

			TagException(const std::string &tag, const std::string &value) :
				tag(tag), value(value) {}
			TagException(const std::string &tag) : tag(tag), value("") {}
			TagException() : tag(""), value("") {}
		};

		/**
		 * Exception thrown when duplicate tags are found. IRCv3 does not allow duplicate keys.
		 */
		class IRCPP_EXPORT TagDuplicateException : public TagException
		{
		public:
			using TagException::TagException;

			const char * what() const throw()
			{
				return "Duplicate tag found";
			}
		};

		/**
		 * Exception thrown when tags are found to be malformed.
		 */
		class IRCPP_EXPORT TagMalformedException : public TagException
		{
		public:
			using TagException::TagException;

			const char * what() const throw()
			{
				return "Malformed tag found";
			}
		};

		/**
		 * Exception thrown when a tag expected was not found
		 */
		class IRCPP_EXPORT TagNotFoundException : public TagException
		{
		public:
			using TagException::TagException;

			const char * what() const throw()
			{
				return "Tags not found";
			}
		};

		/**
		 * Exception thrown when a line is structurally correct but is malformed in another way
		 */
		class IRCPP_EXPORT MalformedLineException : public ParseException
		{
		public:
			const char * what() const throw()
			{
				return "Malformed line";
			}
		};
	}
}

#endif // IRCPP_EXCEPTIONS_HPP
