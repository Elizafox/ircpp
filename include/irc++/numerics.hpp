/**
 * @file irc++/numerics.hpp
 *
 * @brief Numerics for IRC servers.
 *
 * This list was generated automatically using techniques that scraped the server
 * software for them. This work was originally done for the IRCv3 project, but
 * has been commandeered for this use.
 *
 * This file was originally taken from PyIRC and converted to C++, which was
 * created via the above process.
 *
 * It is believed this covers most of the numerics actually used on IRC servers
 * today.
 *
 * There may be aliases as a result of the process. It is recommended to avoid
 * these aliases, and to use the most common name for the numeric.
 *
 * The following IRC servers or standards were checked:
 * - Bahamut (2.0.7)
 * - Charybdis (3.5.0)
 * - ircd-hybrid (7.0)
 * - Inspircd (2.0)
 * - IRCNet ircd (2.11.2)
 * - ircd-seven (1.1.3)
 * - ircu (2.10.12.14)
 * - plexus
 * - ircd-ratbox (3.0.8)
 * - RFC1459
 * - RFC2812
 * - snircd (1.3.4a)
 * - UnrealIRCD (3.2.10.4)
 *
 * @note Not all numerics may be documented, and it may be too difficult to
 *       ever fully document them all.
 */

#ifndef IRCPP_NUMERICS_HPP
#define IRCPP_NUMERICS_HPP

#include "irc++/util.hpp"

namespace irc
{
	namespace numerics
	{
		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WELCOME = "001";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - Inspircd (as RPL_YOURHOSTIS)
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_YOURHOST = "002";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - Inspircd (as RPL_SERVERCREATED)
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_CREATED = "003";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - Inspircd (as RPL_SERVERVERSION)
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_MYINFO = "004";

		/**
		 * This numeric can be found in:
		 * - RFC2812
		 */
		const char * const RPL_BOUNCE_RFC2812 = "005";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ISUPPORT = "005";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 * - UnrealIRCD
		 */
		const char * const RPL_MAP_UNREAL = "006";

		/**
		 * This numeric can be found in:
		 * - Inspircd (as RPL_ENDMAP)
		 * - UnrealIRCD
		 */
		const char * const RPL_MAPEND_UNREAL = "007";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - Inspircd (also as RPL_SNOMASKIS)
		 * - ircd-seven
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_SNOMASK = "008";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - IRCNet ircd (as RPL_BOUNCE)
		 * - plexus
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_REDIR = "010";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const RPL_MAP = "015";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const RPL_MAPMORE = "016";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const RPL_MAPEND = "017";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_MAPSTART = "018";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_HELLO = "020";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_APASSWARN_SET = "030";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_APASSWARN_SECRET = "031";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_APASSWARN_CLEAR = "032";

		/**
		 * This numeric can be found in:
		 * - Inspircd (as RPL_YOURUUID)
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - plexus
		 */
		const char * const RPL_YOURID = "042";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_SAVENICK = "043";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_REMOTEISUPPORT = "105";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACELINK = "200";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACECONNECTING = "201";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACEHANDSHAKE = "202";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACEUNKNOWN = "203";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACEOPERATOR = "204";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACEUSER = "205";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACESERVER = "206";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_TRACECAPTURED = "207";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACESERVICE = "207";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACENEWTYPE = "208";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACECLASS = "209";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSHELP = "210";

		/**
		 * This numeric can be found in:
		 * - RFC2812
		 */
		const char * const RPL_TRACERECONNECT = "210";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSLINKINFO = "211";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSCOMMANDS = "212";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSCLINE = "213";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD (as RPL_STATSOLDNLINE)
		 */
		const char * const RPL_STATSNLINE = "214";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSILINE = "215";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSKLINE = "216";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSPLINE_IRCU = "217";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSQLINE = "217";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSYLINE = "218";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFSTATS = "219";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSBLINE_UNREAL = "220";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_STATSPLINE = "220";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_UMODEIS = "221";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_SQLINE_NICK = "222";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_STATSBLINE_BAHAMUT = "222";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSJLINE = "222";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_STATSELINE_BAHAMUT = "223";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSGLINE_UNREAL = "223";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_STATSFLINE = "224";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSTLINE_UNREAL = "224";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_STATSCLONE = "225";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_STATSDLINE = "225";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSELINE_UNREAL = "225";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - ircu
		 * - plexus
		 * - snircd
		 */
		const char * const RPL_STATSALINE = "226";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_STATSCOUNT = "226";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSNLINE_UNREAL = "226";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_STATSBLINE_PLEXUS = "227";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_STATSGLINE_BAHAMUT = "227";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSVLINE_UNREAL = "227";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSBANVER = "228";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSQLINE_IRCU = "228";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSSPAMF = "229";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSEXCEPTTKL = "230";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_SERVICEINFO = "231";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC1459
		 * - RFC2812
		 */
		const char * const RPL_ENDOFSERVICES = "232";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 * - UnrealIRCD
		 */
		const char * const RPL_RULES = "232";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_SERVICE = "233";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_SERVLIST = "234";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_SERVLISTEND = "235";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSVERBOSE = "236";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSENGINE = "237";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSFLINE_IRCU = "238";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_STATSIAUTH = "239";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const RPL_STATSVLINE = "240";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSLLINE = "241";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSUPTIME = "242";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSOLINE = "243";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSHLINE = "244";

		/**
		 * This numeric can be found in:
		 * - RFC2812
		 */
		const char * const RPL_STATSSLINE_RFC2812 = "244";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSSLINE = "245";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 */
		const char * const RPL_STATSTLINE_HYBRID = "245";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const RPL_STATSPING = "246";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 */
		const char * const RPL_STATSSERVICE = "246";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSTLINE_IRCU = "246";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_STATSULINE_BAHAMUT = "246";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const RPL_STATSBLINE_RFC2812 = "247";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSGLINE_IRCU = "247";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSXLINE = "247";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_STATSDEFINE = "248";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSULINE = "248";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSDEBUG = "249";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSCONN = "250";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const RPL_STATSDLINE_RFC2812 = "250";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LUSERCLIENT = "251";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LUSEROP = "252";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LUSERUNKNOWN = "253";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LUSERCHANNELS = "254";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LUSERME = "255";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ADMINME = "256";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ADMINLOC1 = "257";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ADMINLOC2 = "258";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ADMINEMAIL = "259";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_TRACELOG = "261";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFTRACE = "262";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - ircu
		 * - RFC2812
		 * - snircd
		 */
		const char * const RPL_TRACEEND = "262";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - IRCNet ircd (as RPL_TRYAGAIN)
		 * - plexus
		 * - RFC2812 (as RPL_TRYAGAIN)
		 */
		const char * const RPL_LOAD2HI = "263";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_LOCALUSERS = "265";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_GLOBALUSERS = "266";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_MAPUSERS = "270";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_PRIVS = "270";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_SILELIST = "271";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFSILELIST = "272";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_STATSDLINE_IRCU = "275";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_USINGSSL = "275";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_STATSRLINE = "276";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 */
		const char * const RPL_WHOISCERTFP = "276";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_GLIST = "280";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_ACCEPTLIST = "281";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_ENDOFGLIST = "281";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFACCEPT = "282";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_JUPELIST = "282";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_ENDOFJUPELIST = "283";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_FEATURE = "284";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - snircd
		 */
		const char * const RPL_NEWHOSTIS = "285";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_CHKHEAD = "286";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_CHANUSER = "287";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_PATCHHEAD = "288";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_PATCHCON = "289";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_DATASTR = "290";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_HELPHDR = "290";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_ENDOFCHECK = "291";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_HELPOP = "291";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_HELPTLR = "292";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_HELPHLP = "293";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_HELPFWD = "294";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_HELPIGN = "295";

		/**
		 * RFC's say this is unused; and IRC daemons don't use it either.
		 * It's simply here for completeness.
		 */
		const char * const RPL_NONE = "300";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_AWAY = "301";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_USERHOST = "302";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ISON = "303";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_SYNTAX = "304";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TEXT = "304";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_UNAWAY = "305";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_NOWAWAY = "306";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISREGNICK = "307";

		/**
		 * This numeric can be found in:
		 * - Inspircd (as RPL_RULESTART)
		 * - UnrealIRCD
		 */
		const char * const RPL_RULESSTART = "308";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 */
		const char * const RPL_WHOISADMIN = "308";

		/**
		 * This numeric can be found in:
		 * - Inspircd (as RPL_RULESEND)
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFRULES = "309";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_WHOISSADMIN = "309";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISHELPOP = "310";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - plexus
		 */
		const char * const RPL_WHOISMODES = "310";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_WHOISSVCMSG = "310";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISUSER = "311";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISSERVER = "312";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISOPERATOR = "313";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOWASUSER = "314";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFWHO = "315";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISCHANOP = "316";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISIDLE = "317";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFWHOIS = "318";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISCHANNELS = "319";

		/**
		 * This numeric can be found in:
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISSPECIAL = "320";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LISTSTART = "321";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LIST = "322";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LISTEND = "323";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_CHANNELMODEIS = "324";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_CHANNELMLOCK = "325";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const RPL_UNIQOPIS = "325";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_CHANNELURL = "328";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_CHANNELCREATED = "329";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_CREATIONTIME = "329";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_WHOISACCOUNT = "330";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISLOGGEDIN = "330";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd (also as RPL_NOTOPICSET)
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_NOTOPIC = "331";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TOPIC = "332";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - Inspircd (as RPL_TOPICTIME)
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - IRCNet ircd (as RPL_TOPIC_WHO_TIME)
		 * - ircu
		 * - plexus
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TOPICWHOTIME = "333";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const RPL_COMMANDSYNTAX = "334";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 * - UnrealIRCD (as RPL_LISTSYNTAX)
		 */
		const char * const RPL_LISTUSAGE = "334";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISBOT = "335";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 */
		const char * const RPL_WHOISTEXT = "337";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const RPL_WHOISACTUALLY = "338";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_USERIP = "340";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_INVITING = "341";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_SUMMONING = "342";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_WHOISOPERNAME = "343";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_REOPLIST = "344";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_ENDOFREOPLIST = "345";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_INVITED = "345";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_ISSUEDINVITE = "345";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD (as RPL_INVEXLIST)
		 */
		const char * const RPL_INVITELIST = "346";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD (as RPL_ENDOFINVEXLIST)
		 */
		const char * const RPL_ENDOFINVITELIST = "347";

		/**
		 * This numeric can be found in:
		 * - Bahamut (as RPL_EXEMPTLIST)
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - IRCNet ircd
		 * - plexus
		 * - RFC2812
		 * - UnrealIRCD (as RPL_EXLIST)
		 */
		const char * const RPL_EXCEPTLIST = "348";

		/**
		 * This numeric can be found in:
		 * - Bahamut (as RPL_ENDOFEXEMPTLIST)
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - IRCNet ircd
		 * - plexus
		 * - RFC2812
		 * - UnrealIRCD (as RPL_ENDOFEXLIST)
		 */
		const char * const RPL_ENDOFEXCEPTLIST = "349";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_VERSION = "351";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOREPLY = "352";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_NAMREPLY = "353";

		/**
		 * This numeric can be found in:
		 * - Bahamut (as RPL_RWHOREPLY)
		 */
		const char * const RPL_RWHOREPLY = "354";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_WHOSPCRPL = "354";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_DELNAMREPLY = "355";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_WHOWASREAL = "360";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_KILLDONE = "361";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_CLOSING = "362";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_CLOSEEND = "363";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_LINKS = "364";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFLINKS = "365";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFNAMES = "366";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_BANLIST = "367";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFBANLIST = "368";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFWHOWAS = "369";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_INFO = "371";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_MOTD = "372";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_INFOSTART = "373";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFINFO = "374";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_MOTDSTART = "375";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFMOTD = "376";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISHOST = "378";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISMODES_UNREAL = "379";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - Inspircd (as RPL_YOUAREOPER)
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - IRCNet ircd
		 * - ircu
		 * - plexus
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_YOUREOPER = "381";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_REHASHING = "382";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_YOURESERVICE = "383";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_MYPORTIS = "384";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_NOTOPERANYMORE = "385";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_QLIST = "386";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_RSACHALLENGE = "386";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFQLIST = "387";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_ALIST = "388";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFALIST = "389";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const RPL_TIME = "391";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_USERSSTART = "392";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_USERS = "393";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFUSERS = "394";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const RPL_NOUSERS = "395";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - snircd
		 */
		const char * const RPL_HOSTHIDDEN = "396";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_VISIBLEHOST = "396";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_YOURDISPLAYEDHOST = "396";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_STATSSLINE_SNIRCD = "398";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const RPL_USINGSLINE = "399";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOSUCHNICK = "401";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOSUCHSERVER = "402";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOSUCHCHANNEL = "403";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_CANNOTSENDTOCHAN = "404";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_TOOMANYCHANNELS = "405";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_WASNOSUCHNICK = "406";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_TOOMANYTARGETS = "407";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - plexus
		 */
		const char * const ERR_NOCTRLSONCHAN = "408";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const ERR_NOSUCHSERVICE = "408";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const ERR_SEARCHNOMATCH = "408";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOORIGIN = "409";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - Inspircd (as ERR_INVALIDCAPSUBCOMMAND)
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - ircu (as ERR_UNKNOWNCAPCMD)
		 * - plexus
		 * - snircd (as ERR_UNKNOWNCAPCMD)
		 * - UnrealIRCD
		 */
		const char * const ERR_INVALIDCAPCMD = "410";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NORECIPIENT = "411";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOTEXTTOSEND = "412";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOTOPLEVEL = "413";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_WILDTOPLEVEL = "414";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const ERR_BADMASK = "415";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_QUERYTOOLONG = "416";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const ERR_TOOMANYMATCHES = "416";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_INPUTTOOLONG = "417";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_UNKNOWNCOMMAND = "421";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOMOTD = "422";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOADMININFO = "423";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const ERR_FILERROR = "424";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NOOPERMOTD = "425";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const ERR_TOOMANYAWAY = "429";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NONICKNAMEGIVEN = "431";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd (spelled correctly)
		 * - Bahamut (as ERR_ERRONEUSNICKNAME)
		 * - Charybdis (as ERR_ERRONEUSNICKNAME)
		 * - ircd-hybrid (as ERR_ERRONEUSNICKNAME)
		 * - ircd-seven (as ERR_ERRONEUSNICKNAME)
		 * - ircu (as ERR_ERRONEUSNICKNAME)
		 * - plexus (as ERR_ERRONEUSNICKNAME)
		 * - ircd-ratbox (as ERR_ERRONEUSNICKNAME)
		 * - RFC1459 (as ERR_ERRONEUSNICKNAME)
		 * - RFC2812 (as ERR_ERRONEUSNICKNAME)
		 * - snircd (as ERR_ERRONEUSNICKNAME)
		 * - UnrealIRCD (as ERR_ERRONEUSNICKNAME)
		 */
		const char * const ERR_ERRONEOUSNICKNAME = "432";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NICKNAMEINUSE = "433";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NORULES = "434";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const ERR_SERVICENAMEINUSE = "434";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const ERR_BANNICKCHANGE_CHARYBDIS = "435";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const ERR_BANONCHAN = "435";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - UnrealIRCD
		 */
		const char * const ERR_SERVICECONFUSED = "435";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NICKCOLLISION = "436";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircu
		 * - plexus
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_BANNICKCHANGE = "437";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC2812
		 */
		const char * const ERR_UNAVAILRESOURCE = "437";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - snircd
		 * - UnrealIRCD (as ERR_NCHANGETOOFAST)
		 */
		const char * const ERR_NICKTOOFAST = "438";

		/**
		 * This numeric can be found in:
		 * - Bahamut (as ERR_TARGETTOFAST)
		 * - ircu
		 * - plexus
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_TARGETTOOFAST = "439";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_SERVICESDOWN = "440";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_USERNOTINCHANNEL = "441";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOTONCHANNEL = "442";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_USERONCHANNEL = "443";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const ERR_NOLOGIN = "444";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const ERR_SUMMONDISABLED = "445";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - UnrealIRCD
		 */
		const char * const ERR_USERSDISABLED = "446";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 * - Inspircd (as ERR_CANTCHANGENICK)
		 */
		const char * const ERR_NONICKCHANGE = "447";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOTREGISTERED = "451";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_HOSTILENAME = "455";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_ACCEPTFULL = "456";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_ACCEPTEXIST = "457";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_ACCEPTNOT = "458";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NOHIDING = "459";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NOTFORHALFOPS = "460";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NEEDMOREPARAMS = "461";

		/**
		 * This numeric can be found in:
		 * - Bahamut (as ERR_ALREADYREGISTRED)
		 * - Charybdis (as ERR_ALREADYREGISTRED)
		 * - Inspircd (spelled correctly)
		 * - ircd-hybrid (as ERR_ALREADYREGISTRED)
		 * - ircd-ratbox (as ERR_ALREADYREGISTRED)
		 * - ircd-seven (as ERR_ALREADYREGISTRED)
		 * - ircu (as ERR_ALREADYREGISTRED)
		 * - plexus (as ERR_ALREADYREGISTRED)
		 * - RFC1459 (as ERR_ALREADYREGISTRED)
		 * - RFC2812 (as ERR_ALREADYREGISTRED)
		 * - snircd (as ERR_ALREADYREGISTRED)
		 * - UnrealIRCD (as ERR_ALREADYREGISTRED)
		 */
		const char * const ERR_ALREADYREGISTERED = "462";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOPERMFORHOST = "463";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_PASSWDMISMATCH = "464";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_YOUREBANNEDCREEP = "465";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_YOUWILLBEBANNED = "466";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_KEYSET = "467";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_INVALIDUSERNAME = "468";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const ERR_ONLYSERVERSCANCHANGE = "468";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_LINKSET = "469";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_LINKCHANNEL = "470";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 */
		const char * const ERR_OPERONLYCHAN = "470";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_CHANNELISFULL = "471";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_UNKNOWNMODE = "472";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_INVITEONLYCHAN = "473";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_BANNEDFROMCHAN = "474";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_BADCHANNELKEY = "475";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_BADCHANMASK = "476";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const ERR_OPERONLYCHAN_PLEXUS = "476";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NEEDREGGEDNICK = "477";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const ERR_NOCHANMODES = "477";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_BANLISTFULL = "478";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const ERR_BADCHANNAME = "479";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_LINKFAIL = "479";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_CANNOTKNOCK = "480";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - ircd-ratbox
		 */
		const char * const ERR_SSLONLYCHAN = "480";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const ERR_THROTTLE = "480";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOPRIVILEGES = "481";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_CHANOPRIVSNEEDED = "482";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_CANTKILLSERVER = "483";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_ATTACKDENY = "484";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const ERR_DESYNC = "484";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircu
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const ERR_ISCHANSERVICE = "484";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - RFC2812
		 */
		const char * const ERR_RESTRICTED = "484";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - plexus
		 */
		const char * const ERR_CHANBANREASON = "485";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const ERR_ISREALSERVICE = "485";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_KILLDENY = "485";

		/**
		 * This numeric can be found in:
		 * - RFC2812
		 * - IRCNet ircd (misspelled as ERR_UNIQOPRIVSNEEDED)
		 */
		const char * const ERR_UNIQOPPRIVSNEEDED = "485";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const ERR_ACCOUNTONLY = "486";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const ERR_NONONREG = "486";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const ERR_MSGSERVICES = "487";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NOTFORUSERS = "487";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_HTMDISABLED = "488";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const ERR_NOSSL = "488";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_SECUREONLYCHAN = "489";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircu
		 * - ircd-ratbox
		 * - snircd
		 */
		const char * const ERR_VOICENEEDED = "489";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_ALLMUSTSSL = "490";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NOSWEAR = "490";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_NOOPERHOST = "491";

		/**
		 * This numeric can be found in:
		 * - Inspircd (also has ERR_NOCTCPALLOWED as an alias)
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const ERR_NOCTCP = "492";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 * - RFC1459
		 * - RFC2812
		 */
		const char * const ERR_NOSERVICEHOST = "492";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_NOFEATURE = "493";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 */
		const char * const ERR_NOSHAREDCHAN = "493";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_BADFEATVALUE = "494";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const ERR_OWNMODE = "494";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_BADLOGTYPE = "495";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_DELAYREJOIN = "495";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_BADLOGSYS = "496";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_BADLOGVALUE = "497";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_ISOPERLCHAN = "498";

		/**
		 * This numeric can be found in:
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const ERR_CHANOWNPRIVNEEDED = "499";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const ERR_STATSKLINE_RFC2812 = "499";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_TOOMANYJOINS = "500";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - Inspircd (as ERR_UNKNOWNSNOMASK)
		 * - ircd-hybrid
		 * - ircd-ratbox
		 * - ircd-seven
		 * - IRCNet ircd
		 * - ircu
		 * - plexus
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_UMODEUNKNOWNFLAG = "501";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - Inspircd
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircu
		 * - plexus
		 * - ircd-ratbox
		 * - RFC1459
		 * - RFC2812
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_USERSDONTMATCH = "502";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_GHOSTEDCLIENT = "503";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_USERNOTONSERV = "504";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_SILELISTFULL = "511";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_NOSUCHGLINE = "512";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const ERR_TOOMANYWATCH = "512";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_BADPING = "513";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NEEDPONG = "513";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_WRONGPONG = "513";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_NOSUCHJUPE = "514";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const ERR_TOOMANYDCC = "514";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_BADEXPIRE = "515";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_DONTCHEAT = "516";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircu
		 * - snircd
		 * - UnrealIRCD
		 */
		const char * const ERR_DISABLED = "517";

		/**
		 * This numeric can be found in:
		 * - ircd-hybrid
		 * - ircu
		 * - plexus
		 * - snircd
		 */
		const char * const ERR_LONGMASK = "518";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_NOINVITE = "518";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_ADMONLY = "519";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_TOOMANYUSERS = "519";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_MASKTOOWIDE = "520";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 * - Inspircd (as ERR_CANTJOINOPERSONLY)
		 */
		const char * const ERR_OPERONLY = "520";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const ERR_LISTSYNTAX = "521";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const ERR_WHOSYNTAX = "522";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const ERR_WHOLIMEXCEED = "523";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_HELPNOTFOUND = "524";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const ERR_OPERSPVERIFY = "524";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_QUARANTINED = "524";

		/**
		 * This numeric can be found in:
		 * - ircu
		 */
		const char * const ERR_INVALIDKEY = "525";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const ERR_BADHOSTMASK = "530";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_CANTSENDTOUSER = "531";

		/**
		 * This numeric can be found in:
		 * - snircd
		 */
		const char * const ERR_HOSTUNAVAIL = "531";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_NOTLOWEROPLEVEL = "560";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_NOTMANAGER = "561";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_CHANSECURED = "562";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_UPASSSET = "563";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_UPASSNOTSET = "564";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_NOMANAGER = "566";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_UPASS_SAME_APASS = "567";

		/**
		 * This numeric can be found in:
		 * - ircu
		 * - snircd
		 */
		const char * const ERR_LASTERROR = "568";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_REAWAY = "597";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_GONEAWAY = "598";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_NOTAWAY = "599";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_LOGON = "600";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_LOGOFF = "601";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_WATCHOFF = "602";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_WATCHSTAT = "603";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_NOWON = "604";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_NOWOFF = "605";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_WATCHLIST = "606";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - ircd-hybrid
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFWATCHLIST = "607";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_CLEARWATCH = "608";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_NOWISAWAY = "609";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_MAPMORE_UNREAL = "610";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const RPL_DCCSTATUS = "617";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const RPL_DCCLIST = "618";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const RPL_ENDOFDCCLIST = "619";

		/**
		 * This numeric can be found in:
		 * - Bahamut
		 * - UnrealIRCD
		 */
		const char * const RPL_DCCINFO = "620";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_DUMPING = "640";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_DUMPRPL = "641";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_EODUMP = "642";

		/**
		 * This numeric can be found in:
		 * - UnrealIRCD
		 */
		const char * const RPL_SPAMCMDFWD = "659";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const RPL_STARTTLS = "670";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircd-ratbox
		 * - UnrealIRCD
		 */
		const char * const RPL_WHOISSECURE = "671";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_WHOISSSL = "671";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_WHOISCGI = "672";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_STARTTLS = "691";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_COMMANDS = "702";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_MODLIST = "702";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_COMMANDSEND = "703";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFMODLIST = "703";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_HELPSTART = "704";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_HELPTXT = "705";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFHELP = "706";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const ERR_TARGCHANGE = "707";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - IRCNet ircd
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_ETRACEFULL = "708";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_ETRACE = "709";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_KNOCK = "710";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_KNOCKDLVR = "711";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_TOOMANYKNOCK = "712";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_CHANOPEN = "713";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_KNOCKONCHAN = "714";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_KNOCKDISABLED = "715";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 * - ircd-hybrid (as RPL_TARGUMODEG)
		 * - plexus (as RPL_TARGUMODEG)
		 */
		const char * const ERR_TARGUMODEG = "716";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_TARGNOTIFY = "717";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_UMODEGMSG = "718";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_OMOTDSTART = "720";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_OMOTD = "721";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFOMOTD = "722";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-hybrid
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const ERR_NOPRIVS = "723";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_TESTMASK = "724";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_TESTLINE = "725";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - plexus
		 * - ircd-ratbox
		 */
		const char * const RPL_NOTESTLINE = "726";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_ISCAPTURED = "727";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_TESTMASKGECOS = "727";

		/**
		 * This numeric can be found in:
		 * - plexus
		 */
		const char * const RPL_ISUNCAPTURED = "728";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_QUIETLIST = "728";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_ENDOFQUIETLIST = "729";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_MONONLINE = "730";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_MONOFFLINE = "731";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_MONLIST = "732";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFMONLIST = "733";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const ERR_MONLISTFULL = "734";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_RSACHALLENGE2 = "740";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - ircd-ratbox
		 */
		const char * const RPL_ENDOFRSACHALLENGE2 = "741";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_MLOCKRESTRICTED = "742";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const ERR_INVALIDBAN = "743";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const ERR_TOPICLOCK = "744";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_SCANMATCHED = "750";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_SCANUMODES = "751";

		/**
		 * This numeric can be found in:
		 * - IRCNet ircd
		 */
		const char * const RPL_ETRACEEND = "759";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const RPL_LOGGEDIN = "900";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const RPL_LOGGEDOUT = "901";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_NICKLOCKED = "902";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const RPL_SASLSUCCESS = "903";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_SASLFAIL = "904";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_SASLTOOLONG = "905";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_SASLABORTED = "906";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 * - UnrealIRCD
		 */
		const char * const ERR_SASLALREADY = "907";

		/**
		 * This numeric can be found in:
		 * - Charybdis
		 * - ircd-seven
		 */
		const char * const RPL_SASLMECHS = "908";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_AUTOOPLIST = "910";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_ENDOFAUTOOPLIST = "911";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_WORDFILTERED = "936";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_ENDOFSPAMFILTERLIST = "941";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_SPAMFILTERLIST = "940";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_EXEMPTCHANOPSLIST = "953";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_ENDOFEXEMPTCHANOPSLIST = "954";

		/**
		 * This numeric can be found in:
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const ERR_CANNOTDOCOMMAND = "972";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_CANTUNLOADMODULE = "972";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_UNLOADEDMODULE = "973";

		/**
		 * This numeric can be found in:
		 * - plexus
		 * - UnrealIRCD
		 */
		const char * const ERR_CANNOTCHANGECHANMODE = "974";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const ERR_CANTLOADMODULE = "974";

		/**
		 * This numeric can be found in:
		 * - Inspircd
		 */
		const char * const RPL_LOADEDMODULE = "975";
	}
}

#endif // IRCPP_NUMERICS_HPP
