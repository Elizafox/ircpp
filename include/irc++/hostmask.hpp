/**
 * @file irc++/hostmask.hpp
 *
 * Hostmask parsing and storage
 */

#ifndef IRCPP_HOSTMASK_HPP
#define IRCPP_HOSTMASK_HPP

#include "irc++/util.hpp"

#include <string>
#include <sstream>
#include <utility>

namespace irc
{
	namespace line
	{
		/**
		 * @brief Parsing and storage of hostmasks
		 *
		 * Hostmasks specify the destination or source of a message.
		 */
		class IRCPP_EXPORT Hostmask
		{
		public:
			std::string nick;	///< Nickname this hostmask
			std::string user;	///< Username or ident of this hostmask
			std::string host;	///< @brief Hostname of this hostmask
						///< @note For server messages, only this member is set

			virtual std::string str() = 0;
			virtual bool empty();

			Hostmask & operator=(const Hostmask &) = default;
			Hostmask & operator=(Hostmask &&) = default;

			Hostmask(const std::string &nick, const std::string &user, const std::string &host) :
				nick(nick), user(user), host(host) {}
			Hostmask() = default;
			Hostmask(const Hostmask &) = default;
			Hostmask(Hostmask &&) = default;

			virtual ~Hostmask() {}
		};

		/**
		 * @brief RFC1459 compliant hostmasks
		 *
		 * This is the most common format on the Internet today and the de-facto standard.
		 */
		class IRCPP_EXPORT RFCHostmask : public Hostmask
		{
		public:
			using Hostmask::Hostmask;

			std::string str();

			RFCHostmask & operator=(const RFCHostmask &) = default;
			RFCHostmask & operator=(RFCHostmask &&) = default;

			/**
			 * Construct a hostmask from a string
			 *
			 * This parses the hostmask into its constituent components.
			 */
			explicit RFCHostmask(std::istream &&is);
			explicit RFCHostmask(const std::string &mask) : RFCHostmask(std::istringstream(mask)) {}
			RFCHostmask() = default;
			RFCHostmask(const RFCHostmask &) = default;
			RFCHostmask(RFCHostmask &&) = default;

			virtual ~RFCHostmask() {}
		};
	}
}

#endif // IRCPP_HOSTMASK_HPP
