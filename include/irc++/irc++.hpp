#ifndef IRCPP_IRCPP_HPP
#define IRCPP_IRCPP_HPP

// Include utility headers
#include "irc++/util.hpp"

// Exceptions
#include "irc++/exceptions.hpp"

// Line parsing functions
#include "irc++/hostmask.hpp"
#include "irc++/tags.hpp"
#include "irc++/line.hpp"
#include "irc++/numerics.hpp"

// Higher level API pieces
#include "irc++/action.hpp"
#include "irc++/connection.hpp"

// iostream overloads
#include "irc++/iostream.hpp"

#endif //IRCPP_IRCPP_HPP
