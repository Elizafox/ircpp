/**
 * @file irc++/tags.hpp
 *
 * @brief Tag parsing and storage
 */

#ifndef IRCPP_TAGS_HPP
#define IRCPP_TAGS_HPP

#include "irc++/util.hpp"

#include <string>
#include <unordered_map>
#include <sstream>

namespace irc
{
	namespace line
	{
		/**
		 * @brief Parsing and storage of tags
		 *
		 * Tags are parsed and stored in an `unordered_map` internally.
		 * Iterators and the `[]` operator are provided for convenience.
		 */
		class IRCPP_EXPORT Tags
		{
		private:
			std::unordered_map<std::string, std::string> tags;
		public:
			using KeyValues = decltype(tags);

			void insert(const std::string &tag, const std::string &value);
			void erase(const std::string &tag);
			std::string & find(const std::string &tag);

			typedef decltype(tags)::iterator iterator;
			typedef decltype(tags)::const_iterator const_iterator;

			iterator begin() { return tags.begin(); }
			iterator end() { return tags.end(); }

			std::string & operator[](std::string &tag) { return tags[tag]; }

			virtual std::string str() = 0;
			virtual bool empty();

			Tags & operator=(const Tags &) = default;
			Tags & operator=(Tags &&) = default;

			Tags(KeyValues &tags) : tags(tags) {}
			Tags() = default;
			Tags(const Tags &) = default;
			Tags(Tags &&) = default;

			virtual ~Tags() {}
		};

		/**
		 * Parsing and storage of tags (RFC1459)
		 *
		 * This is the de-facto standard on the Internet for IRC tags.
		 */
		class IRCPP_EXPORT RFCTags : public Tags
		{
		public:
			using Tags::Tags;

			virtual std::string str();

			RFCTags & operator=(const RFCTags &) = default;
			RFCTags & operator=(RFCTags &&) = default;

			explicit RFCTags(std::istream &&is);
			explicit RFCTags(const std::string &raw) : RFCTags(std::istringstream(raw)) {}
			RFCTags() = default;
			RFCTags(const RFCTags &) = default;
			RFCTags(RFCTags &&) = default;
		};
	}
}

#endif // IRCPP_TAGS_HPP
