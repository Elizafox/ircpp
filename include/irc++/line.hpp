/**
 * @file irc++/line.hpp
 *
 * Line parsing and storage
 */

#ifndef IRCPP_LINE_HPP
#define IRCPP_LINE_HPP

#include "irc++/util.hpp"

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <utility>
#include <sstream>

#include "irc++/tags.hpp"
#include "irc++/hostmask.hpp"
#include "irc++/exceptions.hpp"

namespace irc
{
	namespace line
	{
		/**
		 * Base class for parsing and storage of IRC lines
		 */
		class IRCPP_EXPORT Line
		{
		public:
			using TagsType = Tags;
			using HostmaskType = Hostmask;
			using ParametersType = std::vector<std::string>;

			std::string command;		///< IRC command (verb)
			ParametersType parameters;	///< Parameters of the line stored
							///< in order.
							///< @note Only the final parameter
							///<       may contain spaces.

			std::unique_ptr<Tags> tags;
			std::unique_ptr<Hostmask> hostmask;

			virtual std::string str() = 0;
			virtual bool empty() { return command.empty() && parameters.size() == 0; };

			Line & operator=(const Line &) = default;
			Line & operator=(Line &&) = default;

			Line(const Line &) = default;
			Line(Line &&) = default;

			explicit Line(std::unique_ptr<Tags> tags,
				const std::unique_ptr<Hostmask> hostmask,
				const std::string &command, const ParametersType &parameters) :
				command(command), parameters(parameters) {}
			Line() = default;

			virtual ~Line() {}
		};

		/**
		 * Class for parsing and storing RFC1459-compliant lines.
		 *
		 * This is the de-facto standard on the Internet for IRC lines.
		 */
		class IRCPP_EXPORT RFCLine : public Line
		{
		public:
			using TagsType = RFCTags;
			using HostmaskType = RFCHostmask;

			std::string str();
			bool empty();

			/**
			 * @brief Given a raw IRC line, parse into a line structure
			 *
			 * This parses a raw string into an RFCLine object.
			 *
			 * @param raw Raw string to parse
			 * @throw MalformedLineException Line parsed but didn't have a command
			 * @throw TagMalformedException An ill-formed tag was found
			 * @throw TagDuplicateException Multiple tags of the same key were found
			 */
			explicit RFCLine(const std::string &raw) : RFCLine(std::istringstream(raw)) {}

			/**
			 * @brief Given a raw IRC line in an `istream`, parse into a line structure
			 *
			 * This parses data from an `istream` into an RFCLine object.
			 *
			 * @param is `istream` to obtain data from for parsing
			 * @throw MalformedLineException Line parsed but didn't have a command
			 * @throw TagMalformedException An ill-formed tag was found
			 * @throw TagDuplicateException Multiple tags of the same key were found
			 */
			explicit RFCLine(std::istream &&is);

			/**
			 * @brief Create an RFCLine from constituent objects of the line.
			 *
			 * @param tags Tags to use
			 * @param hostmask Hostmask to use
			 * @param command Command to use
			 * @param parameters Parameters to use
			 */
			explicit RFCLine(const RFCTags &tags, const RFCHostmask &hostmask,
				const std::string &command, const ParametersType &parameters) :
				Line::Line(std::make_unique<RFCTags>(tags),
					std::make_unique<RFCHostmask>(hostmask), command,
					parameters)
			{}

			/**
			 * @brief Create an RFCLine from constituent objects of the line.
			 *
			 * @note Tags are left blank by this constructor
			 *
			 * @param hostmask Hostmask to use
			 * @param command Command to use
			 * @param parameters Parameters to use
			 */
			explicit RFCLine(const RFCHostmask &hostmask, const std::string &command,
				const ParametersType &parameters) :
				Line::Line(std::make_unique<RFCTags>(nullptr),
					std::make_unique<RFCHostmask>(hostmask), command,
					parameters)
			{}

			/**
			 * @brief Create an RFCLine from constituent objects of the line.
			 *
			 * @note Tags and hostmask are left blank by this constructor
			 *
			 * @param command Command to use
			 * @param parameters Parameters to use
			 */
			explicit RFCLine(const std::string &command, const ParametersType &parameters) :
				Line::Line(std::make_unique<RFCTags>(nullptr),
					std::make_unique<RFCHostmask>(nullptr), command,
					parameters)
			{}

			RFCLine & operator=(const RFCLine &) = default;
			RFCLine & operator=(RFCLine &&) = default;

			RFCLine() = default;
			RFCLine(const RFCLine &) = default;
			RFCLine(RFCLine &&) = default;

			virtual ~RFCLine() {}
		};
	}
}

#endif // IRCPP_LINE_HPP
