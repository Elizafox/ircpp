/**
 * @file irc++/connection.hpp
 *
 * Storage of connection state.
 */

#ifndef IRCPP_CONNECTION_HPP
#define IRCPP_CONNECTION_HPP

#include "irc++/util.hpp"

#include <string>
#include <locale>

#include "irc++/line.hpp"
#include "irc++/tags.hpp"
#include "irc++/hostmask.hpp"
#include "irc++/action.hpp"

namespace irc
{
	namespace connection
	{
		/**
		 * An abstract base class to implement connection state and other interesting things.
		 */
		class IRCPP_EXPORT Connection
		{
		private:
			using ActionTable = action::ActionTable;

			ActionTable actions_in;	//< Actions run on ingested lines

			bool connected;		//< Set when we finish connecting
			bool registered;	//< Set upon registration with the server
		public:
			/**
			 * See if we are connected yet
			 *
			 * @return true if connected, false if not
			 */
			inline bool is_connected()
			{
				return connected;
			}

			/**
			 * See if we are registered yet
			 *
			 * @return true if registered, false if not
			 */
			inline bool is_registered()
			{
				return registered;
			}

			/**
			 * Sets us as connected
			 */
			inline void set_connected()
			{
				connected = true;
			}

			/**
			 * @brief Sets us as registered with the server
			 *
			 * This has nothing to do with NickServ registration; rather, this is in the
			 * RFC1459 sense.
			 */
			inline void set_registered()
			{
				registered = true;
			}

			/**
			 * Execute a function based on a given line incoming. This calls into the
			 * `ActionTable` class.
			 */
			virtual void execute_in(line::Line &line)
			{
				if(registered)
					actions_in.execute_registered(line);
				else
					actions_in.execute_unregistered(line);
			}

			/**
			 * Add an incoming action instance.
			 *
			 * @warning Do not allow inserted actions to go out of scope or be freed or
			 *          you will be sorry!
			 *
			 * @param command Command this action acts on
			 * @param action Action that is triggered when this command is received
			 */
			inline void add_action_in(const std::string &command,
				action::Action &action)
			{
				actions_in.add_action(command, action);
			}

			/**
			 * Add an incoming action instance before another action
			 *
			 * If the command is not found, simply add to the start of the list
			 *
			 * @warning Do not allow inserted actions to go out of scope or be freed or
			 *          you will be sorry!
			 *
			 * @param command Command this action acts on
			 * @param action Action that is triggered when this command is received
			 * @param before Action we should execute before
			 */
			inline void add_action_in_before(const std::string &command,
				action::Action &action, const action::Action &before)
			{
				actions_in.add_action_before(command, action, before);
			}

			/**
			 * Add an incoming action instance after another action
			 *
			 * If the command is not found, simply add to the end of the list
			 *
			 * @warning Do not allow inserted actions to go out of scope or be freed or
			 *          you will be sorry!
			 *
			 * @param command Command this action acts on
			 * @param action Action that is triggered when this command is received
			 * @param after Action we should execute after
			 */
			inline void add_action_in_after(const std::string &command,
				action::Action &action, const action::Action &after)
			{
				actions_in.add_action_after(command, action, after);
			}

			/**
			 * Delete all the incoming actions for a command.
			 *
			 * @param command Command to delete all actions for
			 */
			inline void del_action_in(const std::string &command)
			{
				actions_in.del_action(command);
			}

			/**
			 * Delete an incoming action for a command.
			 *
			 * @param Command Command to delete action for
			 * @param action Action to delete
			 */
			inline void del_action_in(const std::string &command, action::Action &action)
			{
				actions_in.del_action(command, action);
			}

			/**
			 * Send data out onto the wire in an implementation-defined manner
			 *
			 * @param data Data to send
			 */
			virtual void send_data(const std::string &data) = 0;

			/**
			 * Send a message onto the wire using `send_data`
			 *
			 * @param line Line to send
			 */
			virtual void send_message(line::Line &line)
			{
				const std::string data(line.str());
				send_data(data);
			}

			// You'll want to reimplement the below for other line types

			virtual void send_message(line::RFCTags &tags, line::RFCHostmask &hostmask,
				const std::string &command, const line::Line::Parameters &parameters)
			{
				line::RFCLine line(tags, hostmask, command, parameters);
				send_message(line);
			}

			virtual void send_message(line::RFCHostmask &hostmask,
				const std::string &command, const line::Line::Parameters &parameters)
			{
				line::RFCLine line(hostmask, command, parameters);
				send_message(line);
			}

			virtual void send_message(const std::string &command,
				const line::Line::Parameters &parameters)
			{
				line::RFCLine line(command, parameters);
				send_message(line);
			}

			Connection & operator=(const Connection &) = default;
			Connection & operator=(Connection &&) = default;

			Connection() = default;
			Connection(const Connection &) = default;
			Connection(Connection &&) = default;

			virtual ~Connection() {}
		};
	}
}

#endif // IRCPP_CONNECTION_HPP
