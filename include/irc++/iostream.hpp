/**
 * @file irc++/iostream.hpp
 *
 * Defines iostream wrappers for irc++ structures.
 */

#ifndef IRCPP_IOSTREAM_H
#define IRCPP_IOSTREAM_H

#include "irc++/util.hpp"

#include "irc++/hostmask.hpp"
#include "irc++/tags.hpp"
#include "irc++/line.hpp"

#include <istream>
#include <ostream>

IRCPP_EXPORT std::ostream & operator<<(std::ostream &os, irc::line::Hostmask &hostmask);
IRCPP_EXPORT std::istream & operator>>(std::istream &is, irc::line::RFCHostmask &hostmask);

IRCPP_EXPORT std::ostream & operator<<(std::ostream &os, irc::line::Line &line);
IRCPP_EXPORT std::istream & operator>>(std::istream &is, irc::line::RFCLine &line);

IRCPP_EXPORT std::ostream & operator<<(std::ostream &os, irc::line::Tags &tags);
IRCPP_EXPORT std::istream & operator>>(std::istream &is, irc::line::RFCTags &tags);

#endif //IRCPPIOSTREAM_H
