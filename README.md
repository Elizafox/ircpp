# irc++
An IRC line parser I made in C++ to relearn C++ after not using it for 6 years.

# Why?
See introduction.

# But there's already X/Y/Z
I don't care. This is a learning exercise; I'm writing what I know. Besides,
in my experience most IRC parsers suck, and I see no reason to believe existing
C++ parsers will be magically better just because they added a ++ after the C).

# You aren't following C++ best practises!
Please tell me about it then. I want to improve :). Any style suggestions are
welcome, as well as suggestions about things I can use from the standard library
to make my life easier.

I do want to keep dependencies to a minimum, so please don't suggest external
libraries without very good reason! This library aims to be small and
minimalistic. This also means I don't want to depend on Boost if I don't have
to (I am sure Boost has all kinds of things to help me out, yes, and I am aware
of Boost.Qi, but I just want something small).

**WORD TO THE WISE**: I suffer complaints about indentation and braces poorly (I
use Allman style in C and see no reason to change it now). Any complaints on
that can be resolved by using indent(1).

# API/ABI stability guarantees
This library aims to comply with [semver](http://semver.org/). However, nothing
is guaranteed until 1.0.0. Don't hold your breath on this; this is primarily a
toy to re-learn C++. Expect many changes along the way (or none, if I get
bored).

# Targets
Anything with a C++11 compiler is supported.

# License
This is licensed under the WTFPLv2. Do whatever you want with it. I don't care.
You can even tell me about it, or not, if you prefer.

# Caveats
If it sucks I'm sorry. Also, needs more testing.

# Standards compliance
I have tried to make this RFC1459-compliant (and allow IRCv3 tags), but I
can't promise anything.
