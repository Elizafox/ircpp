/** @file action.cpp
 *
 * Contains functions for the action system. The action system uses functions attached to `Action` class instances, which
 * are then placed into `ActionList`s for reference by-command.
 */
#include <locale>

#include "irc++/action.hpp"

static inline std::string & cmd_lower(std::string &command)
{
	// IRC commands are ASCII.
	static auto ascii_locale = std::locale("C");

	for(auto &c : command)
		c = std::tolower(c, ascii_locale);

	return command;
}

std::atomic<int64_t> irc::action::Action::gid(0);

/**
 * @brief Add an action to the list
 *
 * Adds an action at the end of the action list.
 *
 * @param command Command for the action (will be converted to lowercase for IRC)
 * @param action Action to add
 */
void irc::action::ActionTable::add_action(std::string command, Action &action)
{
	command = cmd_lower(command);
	auto f = actions.find(command);
	if(f == actions.end())
		actions[command] = action_list();

	actions[command].push_back(action);
}

/**
 * @brief Add an action before another action
 *
 * Adds an action after `before` in the action list. If not found, push the action to the front of the action list.
 *
 * @param command Command for the action
 * @param action Action to add
 * @param before The action we are inserting before
 */
void irc::action::ActionTable::add_action_before(std::string command, Action &action, const Action &before)
{
	command = cmd_lower(command);
	auto f = actions.find(command);
	if(f == actions.end())
		actions[command] = action_list();

	auto list = actions[command];

	for(auto it = list.begin(); it != list.end(); it++)
	{
		if(it->get().uid == before.uid)
		{
			// Found it!
			list.insert(it, action);
			return;
		}
	}

	list.push_front(action);
}

/**
 * @brief Add an action after another action
 *
 * Adds an action after `after` in the action list. If not found, push the action to the front of the action list.
 *
 * @param command Command for the action
 * @param action Action to add
 * @param after The action we are inserting after
 */
void irc::action::ActionTable::add_action_after(std::string command, Action &action, const Action &after)
{
	command = cmd_lower(command);
	if(actions.find(command) == actions.end())
		actions[command] = action_list();

	auto list = actions[command];

	for(auto it = list.begin(); it != list.end(); it++)
	{
		if(it->get().uid == after.uid)
		{
			// Found it!
			list.insert(++it, action);
			return;
		}
	}

	list.push_back(action);
}

/**
 * Delete all actions associated with the given command. Do nothing if not found.
 *
 * @param command Command to remove
 */
void irc::action::ActionTable::del_action(std::string command)
{
	command = cmd_lower(command);

	auto find = actions.find(command);
	if(find == actions.end())
		return;

	actions.erase(find);
}

/**
 * Delete an action associated with the given command. Do nothing if not found.
 *
 * @param command Command for the action
 * @param action Action to remove
 */
void irc::action::ActionTable::del_action(std::string command, Action &action)
{
	command = cmd_lower(command);
	if(actions.find(command) == actions.end())
		actions[command] = action_list();

	auto list = actions[command];
	for(auto it = list.begin(); it != list.end(); it++)
	{
		if(it->get().uid == action.uid)
		{
			// Found it!
			list.erase(it);
			return;
		}
	}
}

/**
 * Run the execute_registered functions of actions in the list, using the `command` member of the line passed in.
 *
 * @param line Line to run actions for
 */
void irc::action::ActionTable::execute_registered(irc::line::Line &line)
{
	std::string command{line.command};
	command = cmd_lower(command);
	if(actions.find(command) != actions.end())
	{
		for(auto &action : actions[command])
		{
			try
			{
				action.get().execute_registered(line);
			}
			catch(UnregisteredOnlyException &e)
			{
				// Nothing by default at the moment
				// may add a hook here later.
			}
		}
	}

	if(actions.find(ACTION_ANY) != actions.end())
	{
		// FIXME duplicated code!
		for(auto &action : actions[ACTION_ANY])
		{
			try
			{
				action.get().execute_registered(line);
			}
			catch(UnregisteredOnlyException &e)
			{
				// Nothing by default at the moment
				// may add a hook here later.
			}
		}
	}
}

/**
 * Run the execute_unregistered functions of actions in the list, using the `command` member of the line passed in.
 *
 * @param line Line to run actions for
 */
void irc::action::ActionTable::execute_unregistered(irc::line::Line &line)
{
	std::string command{line.command};
	command = cmd_lower(command);
	if(actions.find(command) != actions.end())
	{
		for(auto &action : actions[command])
		{
			try
			{
				action.get().execute_unregistered(line);
			}
			catch(RegisteredOnlyException &e)
			{
				// Nothing by default at the moment
				// may add a hook here later.
			}
		}
	}

	if(actions.find(ACTION_ANY) != actions.end())
	{
		// FIXME duplicated code!
		for(auto &action : actions[ACTION_ANY])
		{
			try
			{
				action.get().execute_unregistered(line);
			}
			catch(RegisteredOnlyException &e)
			{
				// Nothing by default at the moment
				// may add a hook here later.
			}
		}
	}
}
