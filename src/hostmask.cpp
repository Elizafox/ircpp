#include "irc++/hostmask.hpp"

#include <sstream>
#include <utility>

irc::line::RFCHostmask::RFCHostmask(std::istream &&is)
{
	const char eof = std::char_traits<char>::eof();

	std::istream::sentry s(is);
	std::string tmp;
	int i = 0;

	if(s)
	{
		while(is.good())
		{
			char c = is.get();

			if(i++ == 0 && c == ':')
				// Drop leading colon
				continue;

			if(c == '!' && nick.empty())
			{
				nick = tmp;
				tmp.clear();
			}
			else if(c == '@' && (nick.empty() || user.empty()))
			{
				if(nick.empty())
					nick = std::move(tmp);
				else
					user = tmp;

				tmp.clear();
			}
			else if(c == ' ' || c == eof)
			{
				if(nick.empty())
					nick = tmp;
				else
					host = tmp;

				break;
			}
			else
				tmp += c;
		}

	}
}

/**
 * Check if a hostmask is empty (contains nothing).
 *
 * @return `true` if empty, `false if not.
 */
bool irc::line::Hostmask::empty()
{
	return (nick.empty() && user.empty() && host.empty());
}

/**
 * Convert a hostmask into IRC representation
 */
std::string irc::line::RFCHostmask::str()
{
	if(empty())
		return std::string();

	std::ostringstream os;

	if(nick.empty())
	{
		if(!host.empty())
			os << host;
	}
	else
	{
		os << nick;

		if(!user.empty())
			os << '!' << user;

		if(!empty())
			os << '@' << host;
	}

	return os.str();
}
