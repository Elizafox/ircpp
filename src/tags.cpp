#include "irc++/tags.hpp"
#include "irc++/exceptions.hpp"

#include <sstream>

/**
 * @brief Insert a key and value pair as tags
 *
 * Inserts a key and value pair into the tags structure. Each key must be
 * unique, and this function will not overwrite any existing keys.
 *
 * @param tag key to insert
 * @param value value to insert
 * @throw TagDuplicateException Thrown if the tag was already in the structure.
 * @note To overwrite an existing tag, use `irc::line::Tags::erase` first.
 */
void irc::line::Tags::insert(const std::string &tag, const std::string &value)
{
	auto found = tags.find(tag);
	if(found != tags.end())
		throw TagDuplicateException(tag);

	// First installment wins
	tags[tag] = value;
}

/**
 * @brief Remove a key (and associated value)
 *
 * Removes a key and associated value from the tags structure. The key must
 * exist in the structure already.
 *
 * @param tag key to remove
 * @throw TagNotFoundException Thrown if the tag is not in the structure
 */
void irc::line::Tags::erase(const std::string &tag)
{
	auto found = tags.find(tag);
	if(found == tags.end())
		throw TagNotFoundException(tag);

	tags.erase(tag);
}

/**
 * @brief Given a key, return its associated value
 *
 * This finds the value associated with the given key in the tags structure.
 * The key must exist in the structure.
 *
 * @param tag key to search by
 * @return value associated with the key
 * @throw TagNotFoundException Thrown if the tag is not in the structure
 */
std::string & irc::line::Tags::find(const std::string &tag)
{
	auto found = tags.find(tag);
	if(found == tags.end())
		throw TagNotFoundException(tag);

	return tags[tag];
}

/**
 * @brief Check if the tags structure is empty.
 *
 * Checks if there are any keys in the tags structure.
 *
 * @return `true` if keys are in the structure, else `false`.
 */
bool irc::line::Tags::empty()
{
	return tags.size() == 0;
}

/**
 * @brief Convert the tags structure into a string.
 *
 * This returns a tags string in IRC format.
 */
std::string irc::line::RFCTags::str()
{
	if(empty())
		return std::string();

	std::ostringstream os;

	for(auto &tag : *this)
	{
		if(tag.second == "")
			os << tag.first << ";";
		else
			os << tag.first << "=" << tag.second << ";";
	}

	return os.str();
}

irc::line::RFCTags::RFCTags(std::istream &&is)
{
	using irc::line::TagMalformedException;

	std::istream::sentry s(is);
	std::string key, tmp;
	int i = 0;

	if(s)
	{
		while(is.good())
		{
			char c = is.get();

			if(i++ == 0 && c == '@')
				// Drop leading @
				continue;

			if(c == '=')
			{
				// key=value
				key = tmp;
				tmp.clear();
			}
			else if(c == ';' || c == ' ' || c == std::char_traits<char>::eof())
			{
				if(key.empty())
				{
					// No value found, so this must be the key
					// e.g. @foo;
					if(tmp.empty())
					{
						is.setstate(std::ios_base::failbit);
						// No key and no value, this is an error
						throw TagMalformedException();
					}

					key = tmp;
					tmp.clear();
				}

				// tmp has our value
				insert(key, tmp);
				key.clear(); // Ensure no stale junk values

				if(c == ' ')
					// Done parsing
					// NB: EOLs will be caught next iteration
					break;
			}
			else
				tmp += c;
		}
	}
}
