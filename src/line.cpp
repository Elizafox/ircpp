#include "irc++/line.hpp"
#include "irc++/exceptions.hpp"

#include <sstream>

/**
 * Check if a line is empty (contains nothing).
 *
 * @return `true` if empty, `false if not.
 */
bool irc::line::RFCLine::empty()
{
	return (tags->empty() && hostmask->empty() && command.empty() &&
		parameters.size() == 0);
}

/**
 * Convert a line into RFC1459 representation
 */
std::string irc::line::RFCLine::str()
{
	if(command.length() == 0)
		// No command so...
		return std::string();

	std::ostringstream os;

	if(!tags->empty())
		os << '@' << tags->str() << ' ';

	if(!hostmask->empty())
		os << ':' << hostmask->str() << ' ';

	os << command;

	ParametersType &params = parameters;
	if(params.size() > 0)
	{
		os << ' ';

		for(size_t i = 0; i < params.size(); i++)
		{
			std::string &s = params[i];

			if(s.find(' ') != std::string::npos)
				os << ':';

			os << s;

			if(i < (params.size() - 1))
				// Space after parameter
				os << ' ';
		}
	}

	// Terminator
	os << "\r\n";

	return os.str();
}

irc::line::RFCLine::RFCLine(std::istream &&is)
{
	const char eof = std::char_traits<char>::eof();

	std::istream::sentry s(is);

	if(s)
	{
		while(is.good())
		{
			char c = is.peek();

			if(c == eof || c == '\r' || c == '\n')
				break;
			else if(c == '@')
			{
				// Capture the tags
				is.get();
				tags = std::make_unique<RFCTags>(std::forward<std::istream>(is));
			}
			else if(c == ':')
			{
				is.get();
				if(command.empty())
					// Hostmask
					hostmask = std::make_unique<RFCHostmask>(std::forward<std::istream>(is));
				else
				{
					// Eat final parameter
					std::string param;

					while(true)
					{
						c = is.peek();
						if(c == eof || c == '\r' || c == '\n')
							break;

						param += is.get();
					}

					parameters.push_back(param);
					break;
				}
			}
			else if(c == ' ')
			{
				// Ignore ASCII whitespace
				is.get();
				continue;
			}
			else if(command.empty())
				// Eat command
				is >> command;
			else
			{
				// Eat parameter
				std::string param;

				is >> param;
				parameters.push_back(param);
			}
		}
	}

	// Consume trailing whitespace
	while(is.good())
	{
		char c = is.peek();
		if(c == eof || c == '\r' || c == '\n')
			is.get();
		else
			break;
	}


	if(command.empty())
	{
		is.setstate(std::ios::failbit);
		throw irc::line::MalformedLineException();
	}
}
