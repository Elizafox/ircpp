#include "irc++/iostream.hpp"

#include <istream>
#include <ostream>

std::ostream & operator<<(std::ostream& os, irc::line::Hostmask &hostmask)
{
	os << hostmask.str();
	return os;
}

std::istream & operator>>(std::istream &is, irc::line::RFCHostmask &hostmask)
{
	hostmask = irc::line::RFCHostmask(std::forward<std::istream>(is));
	return is;
}

std::ostream & operator<<(std::ostream& os, irc::line::Tags &tags)
{
	os << tags.str();
	return os;
}

std::istream & operator>>(std::istream &is, irc::line::RFCTags &tags)
{
	tags = irc::line::RFCTags(std::forward<std::istream>(is));
	return is;
}

std::ostream & operator<<(std::ostream& os, irc::line::Line &line)
{
	os << line.str();
	return os;
}

std::istream & operator>>(std::istream &is, irc::line::RFCLine &line)
{
	line = irc::line::RFCLine(std::forward<std::istream>(is));
	return is;
}
